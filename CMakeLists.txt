cmake_minimum_required(VERSION 3.10)
project(optimized-vector)

set(CMAKE_CXX_STANDARD 17)

add_library(optimized-vector SHARED vector.tpp vector.h)
set_target_properties(optimized-vector PROPERTIES LINKER_LANGUAGE CXX)

add_executable(testing main.cpp)
target_link_libraries(testing optimized-vector)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++17 -pedantic")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fsanitize=address,undefined -g")
if (CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -D_GLIBCXX_DEBUG")
endif ()
