#include "vector.h"

int main() {
    optimized::vector<int> v1{1, 2, 3};
    optimized::vector<int> v2(v1);
}
